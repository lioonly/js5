// 1)Метод об'єкту - це функція, пов'язана з об'єктом, яка дозволяє виконувати певні дії або операції на цьому об'єкті.
// 2)Властивості об'єкта в JavaScript можуть мати будь-який тип даних, включаючи числа, рядки, булiани, масиви, об'єкти та функції.
// 3)Це означає, що змінні, що містять об'єкти, посилаються на місце в пам'яті, де зберігається сам об'єкт, а не на його копію.

function createNewUser() {
    var firstName = prompt("Введіть ваше ім'я:");
  
    var lastName = prompt("Введіть ваше прізвище:");

    var newUser = {
      firstName: firstName,
      lastName: lastName,
    };
  
    newUser.getLogin = function () {
      return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    };
  
    return newUser;
  }
  
  var user = createNewUser();
  
  var login = user.getLogin();
  
  console.log(login);
  
